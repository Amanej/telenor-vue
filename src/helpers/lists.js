const cell = function(value, icon, tooltip, input, link, action) {
    this.value = value;
    this.icon = icon;
    this.year = tooltip;
    this.input = input;
    this.link = link;
    this.action = action;
}

module.exports = {
    cell
}