const hasReachedBottom = (e) => {
    let maxTop = e.target.scrollHeight-e.target.clientHeight;
    let bottom = ((maxTop - e.target.scrollTop) === 0);
    return bottom
}

export default {
    hasReachedBottom
}