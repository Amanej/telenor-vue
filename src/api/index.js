import axios from 'axios'

const base = "https://cryptic-ocean-68453.herokuapp.com/";
const backendUrl = process.env.VUE_APP_LOGIN_URL;


// /bedrift/minbedrift/api/protected/mobile/simcards?$count=true&$search=&$skip=221&$top=30

const endpoints = {
    subscriptions: {
        get: {
            url: base + "subs/",
            options: {
                page: 0
            }
        },
        search: {
            url: base + "subs/search/?page=0&limit=300&searchQuery=",
            options: {
                query: ''
            }
        }
    },
    simcards: {
        get: {
            url: base + "simcards/",
            options: {
                page: 0
            }
        },
        search: {
            url: base + "simcards/search/?page=0&limit=300&searchQuery=",
            options: {
                query: ''
            }
        }        
    },
    accounts: {
        get: {
            url: base + "accounts/",
            options: {
                page: 0
            }
        },
        search: {
            url: base + "accounts/search/?page=0&limit=300&searchQuery=",
            options: {
                query: ''
            }
        }        
    },
    locations: {
        get: {
            url: base + "locations/",
            options: {
                page: 0
            }
        },
        search: {
            url: base + "locations/search/?page=0&limit=300&searchQuery=",
            options: {
                query: ''
            }
        }        
    },
    companies: {
        get: {
            url: base + "companies/",
            options: {
                page: 0
            }
        },
        search: {
            url: base + "companies/search/?page=0&limit=300&searchQuery=",
            options: {
                query: ''
            }
        }        
    },
    admins: {
        get: {
            url: base + "admins/",
            options: {
                page: 0
            }
        },
        search: {
            url: base + "admins/search/?page=0&limit=300&searchQuery=",
            options: {
                query: ''
            }
        }        
    }
}

const api = {
    subscriptions: {
        get(page=0) {
            return axios.get(`${endpoints.subscriptions.get.url}?page=${page}`)
            .then(r => {
                console.log(' data ',r.data.data);
                return r.data.data
            })
            .catch(e => {
                throw Error(e);
            });
        },
        search(query) {
            return axios.get(`${endpoints.subscriptions.search.url}${query}`)
            .then(r => {
                console.log(' data ',r.data.data);
                return r.data.data
            })
            .catch(e => {
                throw Error(e);
            });
        }
    },
    simcards: {
        get(page=0) {
            return axios.get(`${endpoints.simcards.get.url}?page=${page}`)
            .then(r => {
                console.log(' data ',r.data.data);
                return r.data.data
            })
            .catch(e => {
                throw Error(e);
            });
        },
        search(query) {
            return axios.get(`${endpoints.simcards.search.url}${query}`)
            .then(r => {
                console.log(' data ',r.data.data);
                return r.data.data
            })
            .catch(e => {
                throw Error(e);
            });
        }
    },
    accounts: {
        get(page=0) {
            return axios.get(`${endpoints.accounts.get.url}?page=${page}`)
            .then(r => {
                console.log(' data ',r.data.data);
                return r.data.data
            })
            .catch(e => {
                throw Error(e);
            });
        }
    },
    locations: {
        get(page=0) {
            return axios.get(`${endpoints.locations.get.url}?page=${page}`)
            .then(r => {
                console.log(' data ',r.data.data);
                return r.data.data
            })
            .catch(e => {
                throw Error(e);
            });
        }
    },
    companies: {
        get(page=0) {
            return axios.get(`${endpoints.companies.get.url}?page=${page}`)
            .then(r => {
                console.log(' data ',r.data.data);
                return r.data.data
            })
            .catch(e => {
                throw Error(e);
            });
        },
        search(query) {
            return axios.get(`${endpoints.companies.search.url}${query}`)
            .then(r => {
                console.log(' data ',r.data.data);
                return r.data.data
            })
            .catch(e => {
                throw Error(e);
            });
        }
    },
    admins: {
        get(page=0) {
            return axios.get(`${endpoints.admins.get.url}?page=${page}`)
            .then(r => {
                console.log(' data ',r.data.data);
                return r.data.data
            })
            .catch(e => {
                throw Error(e);
            });
        }
    },
    auth: {
        get() {
            const current = '/sessions/current';
            return axios.get(`${backendUrl}${current}`)
            .then(res => {
                console.log(res)
                //this.$emit('current', res)
                return res
            })
            .catch(e => {
                throw Error(e);
            })    
        }
    }
}


export default api