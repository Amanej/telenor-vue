[
    {
    "id": "32227942",
    "mobileNumber": {
        "number": "48047501",
        "secret": false
    },
    "user": {
        "personUser": {
        "firstName": "LIV BERIT",
        "lastName": "ALBRIGTSEN"
        }
    },
    "firstUserReference": {
        "referenceText": "test"
    },
    "secondUserReference": {
        "referenceText": "test2 example"
    },
    "thirdUserReference": {
        
    },
    "subscriptionType": {
        "type": "Bedrift Total",
        "accessType": "VOICE"
    },
    "suspended": false,
    "productOfferId": "15471",
    "account": {
        "firstAccountReference": {
        "referenceText": "TESTREF1"
        },
        "accountNumber": "9113712"
    },
    "dataSimMobileNumber": {
        "number": "95247047",
        "secret": false
    },
    "status": "no",
    "barred": false,
    "ownerMasterId": "1680055"
    },
    {
    "id": "34089949",
    "mobileNumber": {
        "number": "90765929",
        "secret": false
    },
    "user": {
        "personUser": {
        "firstName": "SVEIN OVE",
        "lastName": "ALBRIGTSEN"
        }
    },
    "firstUserReference": {
        "referenceText": "test"
    },
    "secondUserReference": {
        "referenceText": "test2 example"
    },
    "thirdUserReference": {
        
    },
    "subscriptionType": {
        "type": "Bedrift Total",
        "accessType": "VOICE"
    },
    "suspended": false,
    "productOfferId": "15471",
    "account": {
        "firstAccountReference": {
        "referenceText": "TESTREF1"
        },
        "accountNumber": "9113712"
    },
    "twinSimMobileNumber": {
        "number": "91241412",
        "secret": false
    },
    "dataSimMobileNumber": {
        "number": "580000809608",
        "secret": false
    },
    "status": "no",
    "barred": false,
    "ownerMasterId": "1680055"
    },
    {
    "id": "32245483",
    "mobileNumber": {
        "number": "99309196",
        "secret": false
    },
    "user": {
        "personUser": {
        "firstName": "TORHILD ANNE",
        "lastName": "ALDAL"
        }
    },
    "firstUserReference": {
        "referenceText": "test"
    },
    "secondUserReference": {
        "referenceText": "4503483202 test2"
    },
    "thirdUserReference": {
        
    },
    "subscriptionType": {
        "type": "Bedrift Total",
        "accessType": "VOICE"
    },
    "suspended": false,
    "productOfferId": "15471",
    "account": {
        "firstAccountReference": {
        "referenceText": "WE"
        },
        "accountNumber": "9114963"
    },
    "status": "no",
    "barred": false,
    "ownerMasterId": "1680055"
    },
    {
    "id": "32630051",
    "mobileNumber": {
        "number": "45798581",
        "secret": false
    },
    "user": {
        "personUser": {
        "firstName": "BINYAM LEMA",
        "lastName": "ALEMU"
        }
    },
    "firstUserReference": {
        "referenceText": "test"
    },
    "secondUserReference": {
        "referenceText": "test2 example"
    },
    "thirdUserReference": {
        
    },
    "subscriptionType": {
        "type": "Bedrift Total",
        "accessType": "VOICE"
    },
    "suspended": false,
    "productOfferId": "15471",
    "account": {
        "firstAccountReference": {
        "referenceText": "TESTREF1"
        },
        "accountNumber": "9113712"
    },
    "status": "no",
    "barred": false,
    "ownerMasterId": "1680055"
    },
    {
    "id": "32439137",
    "mobileNumber": {
        "number": "93055465",
        "secret": false
    },
    "user": {
        "personUser": {
        "firstName": "LEIF ANDREAS",
        "lastName": "ALENDAL"
        }
    },
    "firstUserReference": {
        "referenceText": "test"
    },
    "secondUserReference": {
        "referenceText": "test2 example"
    },
    "thirdUserReference": {
        
    },
    "subscriptionType": {
        "type": "Bedrift Total",
        "accessType": "VOICE"
    },
    "suspended": false,
    "productOfferId": "15471",
    "account": {
        "firstAccountReference": {
        "referenceText": "WE"
        },
        "accountNumber": "9114963"
    },
    "status": "no",
    "barred": false,
    "ownerMasterId": "1680055"
    },
    {
    "id": "32239008",
    "mobileNumber": {
        "number": "95132969",
        "secret": false
    },
    "user": {
        "personUser": {
        "firstName": "MATHIAS",
        "lastName": "ALERINI"
        }
    },
    "firstUserReference": {
        "referenceText": "test"
    },
    "secondUserReference": {
        "referenceText": "test2 example"
    },
    "thirdUserReference": {
        
    },
    "subscriptionType": {
        "type": "Bedrift Total",
        "accessType": "VOICE"
    },
    "suspended": false,
    "productOfferId": "15471",
    "account": {
        "firstAccountReference": {
        "referenceText": "FAST ANSATT 9M01"
        },
        "accountNumber": "9113713"
    },
    "status": "no",
    "barred": false,
    "ownerMasterId": "1680055"
    },
    {
    "id": "32229182",
    "mobileNumber": {
        "number": "48147465",
        "secret": false
    },
    "user": {
        "personUser": {
        "firstName": "TURID EIKEBU",
        "lastName": "ALFSEN"
        }
    },
    "firstUserReference": {
        "referenceText": "test"
    },
    "secondUserReference": {
        "referenceText": "test2 example"
    },
    "thirdUserReference": {
        
    },
    "subscriptionType": {
        "type": "Bedrift Total",
        "accessType": "VOICE"
    },
    "suspended": false,
    "productOfferId": "15471",
    "account": {
        "firstAccountReference": {
        "referenceText": "TESTREF1"
        },
        "accountNumber": "9113712"
    },
    "dataSimMobileNumber": {
        "number": "90803575",
        "secret": false
    },
    "status": "no",
    "barred": false,
    "ownerMasterId": "1680055"
    },
    {
    "id": "32239697",
    "mobileNumber": {
        "number": "95206269",
        "secret": false
    },
    "user": {
        "personUser": {
        "firstName": "KJELL HARRY",
        "lastName": "ALFSVÅG"
        }
    },
    "firstUserReference": {
        "referenceText": "test"
    },
    "secondUserReference": {
        "referenceText": "test2 example"
    },
    "thirdUserReference": {
        
    },
    "subscriptionType": {
        "type": "Bedrift Total",
        "accessType": "VOICE"
    },
    "suspended": false,
    "productOfferId": "15471",
    "account": {
        "firstAccountReference": {
        "referenceText": "FAST ANSATT 9M01"
        },
        "accountNumber": "9113713"
    },
    "twinSimMobileNumber": {
        "number": "46942984",
        "secret": false
    },
    "dataSimMobileNumber": {
        "number": "580000700643",
        "secret": false
    },
    "status": "no",
    "barred": false,
    "ownerMasterId": "1680055"
    },
    {
    "id": "32284589",
    "mobileNumber": {
        "number": "90629716",
        "secret": false
    },
    "user": {
        "personUser": {
        "firstName": "KRISTOFFER",
        "lastName": "ALFTHAN"
        }
    },
    "firstUserReference": {
        "referenceText": "test"
    },
    "secondUserReference": {
        "referenceText": "test2 example"
    },
    "thirdUserReference": {
        
    },
    "subscriptionType": {
        "type": "Bedrift Total",
        "accessType": "VOICE"
    },
    "suspended": false,
    "productOfferId": "15471",
    "account": {
        "firstAccountReference": {
        "referenceText": "TESTREF1"
        },
        "accountNumber": "9113712"
    },
    "twinSimMobileNumber": {
        "number": "94862561",
        "secret": false
    },
    "status": "no",
    "barred": false,
    "ownerMasterId": "1680055"
    },
    {
    "id": "32233071",
    "mobileNumber": {
        "number": "90983621",
        "secret": false
    },
    "user": {
        "personUser": {
        "firstName": "ANNBJØRG",
        "lastName": "ALGERØY"
        }
    },
    "firstUserReference": {
        "referenceText": "test"
    },
    "secondUserReference": {
        "referenceText": "test2 example"
    },
    "thirdUserReference": {
        
    },
    "subscriptionType": {
        "type": "Bedrift Total",
        "accessType": "VOICE"
    },
    "suspended": false,
    "productOfferId": "15471",
    "account": {
        "firstAccountReference": {
        "referenceText": "FAST ANSATT 9M01"
        },
        "accountNumber": "9113713"
    },
    "status": "no",
    "barred": false,
    "ownerMasterId": "1680055"
    },
    {
    "id": "32237266",
    "mobileNumber": {
        "number": "92430051",
        "secret": false
    },
    "user": {
        "personUser": {
        "firstName": "BJARTE",
        "lastName": "ALGRØY"
        }
    },
    "firstUserReference": {
        "referenceText": "test"
    },
    "secondUserReference": {
        "referenceText": "test2 example"
    },
    "thirdUserReference": {
        
    },
    "subscriptionType": {
        "type": "Bedrift Total",
        "accessType": "VOICE"
    },
    "suspended": false,
    "productOfferId": "15471",
    "account": {
        "firstAccountReference": {
        "referenceText": "FAST ANSATT 9M01"
        },
        "accountNumber": "9113713"
    },
    "status": "no",
    "barred": false,
    "ownerMasterId": "1680055"
    },
    {
    "id": "32241034",
    "mobileNumber": {
        "number": "95720121",
        "secret": false
    },
    "user": {
        "personUser": {
        "firstName": "KJARTAN",
        "lastName": "ALGRØY"
        }
    },
    "firstUserReference": {
        "referenceText": "test"
    },
    "secondUserReference": {
        "referenceText": "test2 example"
    },
    "thirdUserReference": {
        
    },
    "subscriptionType": {
        "type": "Bedrift Total",
        "accessType": "VOICE"
    },
    "suspended": false,
    "productOfferId": "15471",
    "account": {
        "firstAccountReference": {
        "referenceText": "WE"
        },
        "accountNumber": "9114963"
    },
    "dataSimMobileNumber": {
        "number": "580007654909",
        "secret": false
    },
    "status": "no",
    "barred": false,
    "ownerMasterId": "1680055"
    },
    {
    "id": "35392995",
    "mobileNumber": {
        "number": "93210319",
        "secret": false
    },
    "user": {
        "personUser": {
        "firstName": "OLAV ANDRÉ",
        "lastName": "ALGRØY"
        }
    },
    "firstUserReference": {
        "referenceText": "test"
    },
    "secondUserReference": {
        "referenceText": "test2 example"
    },
    "thirdUserReference": {
        
    },
    "subscriptionType": {
        "type": "Bedrift Total",
        "accessType": "VOICE"
    },
    "suspended": false,
    "productOfferId": "15471",
    "account": {
        "firstAccountReference": {
        "referenceText": "WE"
        },
        "accountNumber": "9114963"
    },
    "status": "no",
    "barred": false,
    "ownerMasterId": "1680055"
    },
    {
    "id": "34352595",
    "mobileNumber": {
        "number": "92087060",
        "secret": false
    },
    "user": {
        "personUser": {
        "firstName": "TORILL ANITA",
        "lastName": "ALGRØY"
        }
    },
    "firstUserReference": {
        "referenceText": "test"
    },
    "secondUserReference": {
        "referenceText": "test2 example"
    },
    "thirdUserReference": {
        
    },
    "subscriptionType": {
        "type": "Bedrift Total",
        "accessType": "VOICE"
    },
    "suspended": false,
    "productOfferId": "15471",
    "account": {
        "firstAccountReference": {
        "referenceText": "FAST ANSATT 9M01"
        },
        "accountNumber": "9113713"
    },
    "status": "no",
    "barred": false,
    "ownerMasterId": "1680055"
    },
    {
    "id": "32232941",
    "mobileNumber": {
        "number": "90899469",
        "secret": false
    },
    "user": {
        "personUser": {
        "firstName": "DANA ZIDE",
        "lastName": "ALI"
        }
    },
    "firstUserReference": {
        "referenceText": "test"
    },
    "secondUserReference": {
        "referenceText": "test2 example"
    },
    "thirdUserReference": {
        
    },
    "subscriptionType": {
        "type": "Bedrift Total",
        "accessType": "VOICE"
    },
    "suspended": false,
    "productOfferId": "15471",
    "account": {
        "firstAccountReference": {
        "referenceText": "FAST ANSATT 9M01"
        },
        "accountNumber": "9113713"
    },
    "twinSimMobileNumber": {
        "number": "91007496",
        "secret": false
    },
    "dataSimMobileNumber": {
        "number": "97138398",
        "secret": false
    },
    "data2SimMobileNumber": {
        "number": "580000611797",
        "secret": false
    },
    "status": "no",
    "barred": false,
    "ownerMasterId": "1680055"
    },
    {
    "id": "32239120",
    "mobileNumber": {
        "number": "95156719",
        "secret": false
    },
    "user": {
        "personUser": {
        "firstName": "MARWAN KAREEM",
        "lastName": "ALI"
        }
    },
    "firstUserReference": {
        "referenceText": "test"
    },
    "secondUserReference": {
        "referenceText": "test2 example"
    },
    "thirdUserReference": {
        
    },
    "subscriptionType": {
        "type": "Bedrift Total",
        "accessType": "VOICE"
    },
    "suspended": false,
    "productOfferId": "15471",
    "account": {
        "firstAccountReference": {
        "referenceText": "FAST ANSATT 9M01"
        },
        "accountNumber": "9113713"
    },
    "status": "no",
    "barred": false,
    "ownerMasterId": "1680055"
    },
    {
    "id": "33436934",
    "mobileNumber": {
        "number": "90039846",
        "secret": false
    },
    "user": {
        "personUser": {
        "firstName": "VAHID",
        "lastName": "ALIPOUR TABRIZY"
        }
    },
    "firstUserReference": {
        "referenceText": "test"
    },
    "secondUserReference": {
        "referenceText": "test2 example"
    },
    "thirdUserReference": {
        
    },
    "subscriptionType": {
        "type": "Bedrift Total",
        "accessType": "VOICE"
    },
    "suspended": false,
    "productOfferId": "15471",
    "account": {
        "firstAccountReference": {
        "referenceText": "FAST ANSATT 9M01"
        },
        "accountNumber": "9113713"
    },
    "status": "no",
    "barred": false,
    "ownerMasterId": "1680055"
    },
    {
    "id": "44301359",
    "mobileNumber": {
        "number": "91241351",
        "secret": false
    },
    "user": {
        "personUser": {
        "firstName": "TAHIR",
        "lastName": "ALI"
        }
    },
    "firstUserReference": {
        "referenceText": "test"
    },
    "secondUserReference": {
        "referenceText": "test2 example"
    },
    "thirdUserReference": {
        
    },
    "subscriptionType": {
        "type": "Bedrift Total",
        "accessType": "VOICE"
    },
    "suspended": false,
    "productOfferId": "15471",
    "account": {
        "firstAccountReference": {
        "referenceText": "WE"
        },
        "accountNumber": "9114963"
    },
    "status": "no",
    "barred": false,
    "ownerMasterId": "1680055"
    },
    {
    "id": "41966173",
    "mobileNumber": {
        "number": "90986271",
        "secret": false
    },
    "user": {
        "personUser": {
        "firstName": "NARGIZ",
        "lastName": "ALIYEVA-HENNINGSEN"
        }
    },
    "firstUserReference": {
        "referenceText": "test"
    },
    "secondUserReference": {
        "referenceText": "test2 example"
    },
    "thirdUserReference": {
        
    },
    "subscriptionType": {
        "type": "Bedrift Total",
        "accessType": "VOICE"
    },
    "suspended": false,
    "productOfferId": "15471",
    "account": {
        "firstAccountReference": {
        "referenceText": "WE"
        },
        "accountNumber": "9114963"
    },
    "status": "no",
    "barred": false,
    "ownerMasterId": "1680055"
    },
    {
    "id": "32231271",
    "mobileNumber": {
        "number": "90717789",
        "secret": false
    },
    "user": {
        "personUser": {
        "firstName": "LALA",
        "lastName": "ALIYEVA"
        }
    },
    "firstUserReference": {
        "referenceText": "test"
    },
    "secondUserReference": {
        "referenceText": "test2 example"
    },
    "thirdUserReference": {
        
    },
    "subscriptionType": {
        "type": "Bedrift Total",
        "accessType": "VOICE"
    },
    "suspended": false,
    "productOfferId": "15471",
    "account": {
        "firstAccountReference": {
        "referenceText": "FAST ANSATT 9M01"
        },
        "accountNumber": "9113713"
    },
    "status": "no",
    "barred": false,
    "ownerMasterId": "1680055"
    },
    {
    "id": "38873887",
    "mobileNumber": {
        "number": "94164138",
        "secret": false
    },
    "user": {
        "personUser": {
        "firstName": "SEVIL",
        "lastName": "ALIYEVA"
        }
    },
    "firstUserReference": {
        "referenceText": "test"
    },
    "secondUserReference": {
        "referenceText": "test2 example"
    },
    "thirdUserReference": {
        
    },
    "subscriptionType": {
        "type": "Bedrift Total",
        "accessType": "VOICE"
    },
    "suspended": false,
    "productOfferId": "15471",
    "account": {
        "firstAccountReference": {
        "referenceText": "WE"
        },
        "accountNumber": "9114963"
    },
    "status": "no",
    "barred": false,
    "ownerMasterId": "1680055"
    },
    {
    "id": "33552345",
    "mobileNumber": {
        "number": "40104066",
        "secret": false
    },
    "user": {
        "personUser": {
        "firstName": "MELANIE LIEVE MARIE",
        "lastName": "ALLAEYS"
        }
    },
    "firstUserReference": {
        "referenceText": "test"
    },
    "secondUserReference": {
        "referenceText": "test2 example"
    },
    "thirdUserReference": {
        
    },
    "subscriptionType": {
        "type": "Bedrift Total",
        "accessType": "VOICE"
    },
    "suspended": false,
    "productOfferId": "15471",
    "account": {
        "firstAccountReference": {
        "referenceText": "FAST ANSATT 9M01"
        },
        "accountNumber": "9113713"
    },
    "status": "no",
    "barred": false,
    "ownerMasterId": "1680055"
    },
    {
    "id": "35359810",
    "mobileNumber": {
        "number": "95422688",
        "secret": false
    },
    "user": {
        "personUser": {
        "firstName": "ELIN OSVÅG",
        "lastName": "ALLEN"
        }
    },
    "firstUserReference": {
        "referenceText": "test"
    },
    "secondUserReference": {
        "referenceText": "test2 example"
    },
    "thirdUserReference": {
        
    },
    "subscriptionType": {
        "type": "Bedrift Total",
        "accessType": "VOICE"
    },
    "suspended": false,
    "productOfferId": "15471",
    "account": {
        "firstAccountReference": {
        "referenceText": "WE"
        },
        "accountNumber": "9114963"
    },
    "status": "no",
    "barred": false,
    "ownerMasterId": "1680055"
    },
    {
    "id": "35130179",
    "mobileNumber": {
        "number": "90364115",
        "secret": false
    },
    "user": {
        "personUser": {
        "firstName": "RICHARD DUNCAN",
        "lastName": "ALLEN"
        }
    },
    "firstUserReference": {
        "referenceText": "test"
    },
    "secondUserReference": {
        "referenceText": "test2 example"
    },
    "thirdUserReference": {
        
    },
    "subscriptionType": {
        "type": "Bedrift Total",
        "accessType": "VOICE"
    },
    "suspended": false,
    "productOfferId": "15471",
    "account": {
        "firstAccountReference": {
        "referenceText": "FAST ANSATT 9M01"
        },
        "accountNumber": "9113713"
    },
    "status": "no",
    "barred": false,
    "ownerMasterId": "1680055"
    },
    {
    "id": "32242855",
    "mobileNumber": {
        "number": "97106153",
        "secret": false
    },
    "user": {
        "personUser": {
        "firstName": "JAN ERIK",
        "lastName": "ALLERS"
        }
    },
    "firstUserReference": {
        "referenceText": "test"
    },
    "secondUserReference": {
        "referenceText": "test2 example"
    },
    "thirdUserReference": {
        
    },
    "subscriptionType": {
        "type": "Bedrift Total",
        "accessType": "VOICE"
    },
    "suspended": false,
    "productOfferId": "15471",
    "account": {
        "firstAccountReference": {
        "referenceText": "WE"
        },
        "accountNumber": "9114963"
    },
    "dataSimMobileNumber": {
        "number": "91588320",
        "secret": false
    },
    "data2SimMobileNumber": {
        "number": "580000078721",
        "secret": false
    },
    "status": "no",
    "barred": false,
    "ownerMasterId": "1680055"
    },
    {
    "id": "32237292",
    "mobileNumber": {
        "number": "91893712",
        "secret": false
    },
    "user": {
        "personUser": {
        "firstName": "DAGFINN",
        "lastName": "ALM"
        }
    },
    "firstUserReference": {
        "referenceText": "test"
    },
    "secondUserReference": {
        "referenceText": "test2 example"
    },
    "thirdUserReference": {
        
    },
    "subscriptionType": {
        "type": "Bedrift Total",
        "accessType": "VOICE"
    },
    "suspended": false,
    "productOfferId": "15471",
    "account": {
        "firstAccountReference": {
        "referenceText": "FAST ANSATT 9M01"
        },
        "accountNumber": "9113713"
    },
    "twinSimMobileNumber": {
        "number": "48152492",
        "secret": false
    },
    "data2SimMobileNumber": {
        "number": "580000570499",
        "secret": false
    },
    "status": "no",
    "barred": false,
    "ownerMasterId": "1680055"
    },
    {
    "id": "32245333",
    "mobileNumber": {
        "number": "99306923",
        "secret": false
    },
    "user": {
        "personUser": {
        "firstName": "ARVID",
        "lastName": "ALME"
        }
    },
    "firstUserReference": {
        "referenceText": "test"
    },
    "secondUserReference": {
        "referenceText": "test2 example"
    },
    "thirdUserReference": {
        
    },
    "subscriptionType": {
        "type": "Bedrift Total",
        "accessType": "VOICE"
    },
    "suspended": false,
    "productOfferId": "15471",
    "account": {
        "firstAccountReference": {
        "referenceText": "WE"
        },
        "accountNumber": "9114963"
    },
    "twinSimMobileNumber": {
        "number": "46849389",
        "secret": false
    },
    "dataSimMobileNumber": {
        "number": "580000252835",
        "secret": false
    },
    "status": "no",
    "barred": false,
    "ownerMasterId": "1680055"
    },
    {
    "id": "32245334",
    "mobileNumber": {
        "number": "99306885",
        "secret": false
    },
    "user": {
        "personUser": {
        "firstName": "JOSTEIN",
        "lastName": "ALME"
        }
    },
    "firstUserReference": {
        "referenceText": "test"
    },
    "secondUserReference": {
        "referenceText": "test2 example"
    },
    "thirdUserReference": {
        
    },
    "subscriptionType": {
        "type": "Bedrift Total",
        "accessType": "VOICE"
    },
    "suspended": false,
    "productOfferId": "15471",
    "account": {
        "firstAccountReference": {
        "referenceText": "WE"
        },
        "accountNumber": "9114963"
    },
    "status": "no",
    "barred": false,
    "ownerMasterId": "1680055"
    },
    {
    "id": "32234172",
    "mobileNumber": {
        "number": "91194609",
        "secret": false
    },
    "user": {
        "personUser": {
        "firstName": "LARS NORMANN",
        "lastName": "ALME"
        }
    },
    "firstUserReference": {
        "referenceText": "test"
    },
    "secondUserReference": {
        "referenceText": "test2 example"
    },
    "thirdUserReference": {
        
    },
    "subscriptionType": {
        "type": "Bedrift Total",
        "accessType": "VOICE"
    },
    "suspended": false,
    "productOfferId": "15471",
    "account": {
        "firstAccountReference": {
        "referenceText": "FAST ANSATT 9M01"
        },
        "accountNumber": "9113713"
    },
    "status": "no",
    "barred": false,
    "ownerMasterId": "1680055"
    },
    {
    "id": "32241460",
    "mobileNumber": {
        "number": "95808160",
        "secret": false
    },
    "user": {
        "personUser": {
        "firstName": "JOHNNY ARNE",
        "lastName": "ALMELID"
        }
    },
    "firstUserReference": {
        "referenceText": "test"
    },
    "secondUserReference": {
        "referenceText": "test2 example"
    },
    "thirdUserReference": {
        
    },
    "subscriptionType": {
        "type": "Bedrift Total",
        "accessType": "VOICE"
    },
    "suspended": false,
    "productOfferId": "15471",
    "account": {
        "firstAccountReference": {
        "referenceText": "WE"
        },
        "accountNumber": "9114963"
    },
    "twinSimMobileNumber": {
        "number": "90796136",
        "secret": false
    },
    "dataSimMobileNumber": {
        "number": "91241937",
        "secret": false
    },
    "status": "no",
    "barred": false,
    "ownerMasterId": "1680055"
    },
    {
        "id": "32225358",
        "mobileNumber": {
          "number": "48022458",
          "secret": false
        },
        "user": {
          "personUser": {
            "firstName": "HENRIK SOLGAARD",
            "lastName": "ANDERSEN"
          }
        },
        "firstUserReference": {
          "referenceText": "test"
        },
        "secondUserReference": {
          "referenceText": "test2 example"
        },
        "thirdUserReference": {
          
        },
        "subscriptionType": {
          "type": "Bedrift Total",
          "accessType": "VOICE"
        },
        "suspended": false,
        "productOfferId": "15471",
        "account": {
          "firstAccountReference": {
            "referenceText": "TESTREF1"
          },
          "accountNumber": "9113712"
        },
        "status": "no",
        "barred": false,
        "ownerMasterId": "1680055"
      },
      {
        "id": "32238029",
        "mobileNumber": {
          "number": "93878793",
          "secret": false
        },
        "user": {
          "personUser": {
            "firstName": "HILDE",
            "lastName": "ANDERSEN"
          }
        },
        "firstUserReference": {
          "referenceText": "test"
        },
        "secondUserReference": {
          "referenceText": "test2 example"
        },
        "thirdUserReference": {
          
        },
        "subscriptionType": {
          "type": "Bedrift Total",
          "accessType": "VOICE"
        },
        "suspended": false,
        "productOfferId": "15471",
        "account": {
          "firstAccountReference": {
            "referenceText": "FAST ANSATT 9M01"
          },
          "accountNumber": "9113713"
        },
        "status": "no",
        "barred": false,
        "ownerMasterId": "1680055"
      },
      {
        "id": "32239104",
        "mobileNumber": {
          "number": "95155713",
          "secret": false
        },
        "user": {
          "personUser": {
            "firstName": "IVAN ROBERT",
            "lastName": "ANDERSEN"
          }
        },
        "firstUserReference": {
          "referenceText": "test"
        },
        "secondUserReference": {
          "referenceText": "test2 example"
        },
        "thirdUserReference": {
          
        },
        "subscriptionType": {
          "type": "Bedrift Total",
          "accessType": "VOICE"
        },
        "suspended": false,
        "productOfferId": "15471",
        "account": {
          "firstAccountReference": {
            "referenceText": "FAST ANSATT 9M01"
          },
          "accountNumber": "9113713"
        },
        "twinSimMobileNumber": {
          "number": "90042328",
          "secret": false
        },
        "status": "no",
        "barred": false,
        "ownerMasterId": "1680055"
      },
      {
        "id": "32237712",
        "mobileNumber": {
          "number": "94156062",
          "secret": false
        },
        "user": {
          "personUser": {
            "firstName": "JAN",
            "lastName": "ANDERSEN"
          }
        },
        "firstUserReference": {
          "referenceText": "test"
        },
        "secondUserReference": {
          "referenceText": "test2 example"
        },
        "thirdUserReference": {
          
        },
        "subscriptionType": {
          "type": "Bedrift Total",
          "accessType": "VOICE"
        },
        "suspended": false,
        "productOfferId": "15471",
        "account": {
          "firstAccountReference": {
            "referenceText": "FAST ANSATT 9M01"
          },
          "accountNumber": "9113713"
        },
        "status": "no",
        "barred": false,
        "ownerMasterId": "1680055"
      },
      {
        "id": "32224812",
        "mobileNumber": {
          "number": "47608989",
          "secret": false
        },
        "user": {
          "personUser": {
            "firstName": "JAN FOLDØY",
            "lastName": "ANDERSEN"
          }
        },
        "firstUserReference": {
          "referenceText": "test"
        },
        "secondUserReference": {
          "referenceText": "test2 example"
        },
        "thirdUserReference": {
          
        },
        "subscriptionType": {
          "type": "Bedrift Total",
          "accessType": "VOICE"
        },
        "suspended": false,
        "productOfferId": "15471",
        "account": {
          "firstAccountReference": {
            "referenceText": "TESTREF1"
          },
          "accountNumber": "9113712"
        },
        "dataSimMobileNumber": {
          "number": "90016264",
          "secret": false
        },
        "data2SimMobileNumber": {
          "number": "580000708046",
          "secret": false
        },
        "status": "no",
        "barred": false,
        "ownerMasterId": "1680055"
      },
      {
        "id": "34670418",
        "mobileNumber": {
          "number": "97004112",
          "secret": false
        },
        "user": {
          "personUser": {
            "firstName": "JAN MAGNE",
            "lastName": "ANDERSEN"
          }
        },
        "firstUserReference": {
          "referenceText": "test"
        },
        "secondUserReference": {
          "referenceText": "test2 example"
        },
        "thirdUserReference": {
          
        },
        "subscriptionType": {
          "type": "Bedrift Total",
          "accessType": "VOICE"
        },
        "suspended": false,
        "productOfferId": "15471",
        "account": {
          "firstAccountReference": {
            "referenceText": "WE"
          },
          "accountNumber": "9114963"
        },
        "twinSimMobileNumber": {
          "number": "94822651",
          "secret": false
        },
        "status": "no",
        "barred": false,
        "ownerMasterId": "1680055"
      },
      {
        "id": "34842965",
        "mobileNumber": {
          "number": "90043139",
          "secret": false
        },
        "user": {
          "personUser": {
            "firstName": "JEANETTE BRYNE",
            "lastName": "ANDERSEN"
          }
        },
        "firstUserReference": {
          "referenceText": "test"
        },
        "secondUserReference": {
          "referenceText": "test2 example"
        },
        "thirdUserReference": {
          
        },
        "subscriptionType": {
          "type": "Bedrift Total",
          "accessType": "VOICE"
        },
        "suspended": false,
        "productOfferId": "15471",
        "account": {
          "firstAccountReference": {
            "referenceText": "TESTREF1"
          },
          "accountNumber": "9113712"
        },
        "status": "no",
        "barred": false,
        "ownerMasterId": "1680055"
      },
      {
        "id": "32239382",
        "mobileNumber": {
          "number": "95076723",
          "secret": false
        },
        "user": {
          "personUser": {
            "firstName": "JOHAN",
            "lastName": "ANDERSEN"
          }
        },
        "firstUserReference": {
          "referenceText": "test"
        },
        "secondUserReference": {
          "referenceText": "test2 example"
        },
        "thirdUserReference": {
          
        },
        "subscriptionType": {
          "type": "Bedrift Total",
          "accessType": "VOICE"
        },
        "suspended": false,
        "productOfferId": "15471",
        "account": {
          "firstAccountReference": {
            "referenceText": "FAST ANSATT 9M01"
          },
          "accountNumber": "9113713"
        },
        "twinSimMobileNumber": {
          "number": "97159679",
          "secret": false
        },
        "status": "no",
        "barred": false,
        "ownerMasterId": "1680055"
      },
      {
        "id": "32236455",
        "mobileNumber": {
          "number": "91860738",
          "secret": false
        },
        "user": {
          "personUser": {
            "firstName": "JON REIDAR",
            "lastName": "ANDERSEN"
          }
        },
        "firstUserReference": {
          "referenceText": "test"
        },
        "secondUserReference": {
          "referenceText": "test2 example"
        },
        "thirdUserReference": {
          
        },
        "subscriptionType": {
          "type": "Bedrift Total",
          "accessType": "VOICE"
        },
        "suspended": false,
        "productOfferId": "15471",
        "account": {
          "firstAccountReference": {
            "referenceText": "FAST ANSATT 9M01"
          },
          "accountNumber": "9113713"
        },
        "twinSimMobileNumber": {
          "number": "48007188",
          "secret": false
        },
        "status": "no",
        "barred": false,
        "ownerMasterId": "1680055"
      },
      {
        "id": "32229160",
        "mobileNumber": {
          "number": "48145495",
          "secret": false
        },
        "user": {
          "personUser": {
            "firstName": "KARI STRAUME",
            "lastName": "ANDERSEN"
          }
        },
        "firstUserReference": {
          "referenceText": "test"
        },
        "secondUserReference": {
          "referenceText": "test2 example"
        },
        "thirdUserReference": {
          
        },
        "subscriptionType": {
          "type": "Bedrift Total",
          "accessType": "VOICE"
        },
        "suspended": false,
        "productOfferId": "15471",
        "account": {
          "firstAccountReference": {
            "referenceText": "TESTREF1"
          },
          "accountNumber": "9113712"
        },
        "status": "no",
        "barred": false,
        "ownerMasterId": "1680055"
      },
      {
        "id": "33883016",
        "mobileNumber": {
          "number": "91858470",
          "secret": false
        },
        "user": {
          "personUser": {
            "firstName": "KATRINE",
            "lastName": "ANDERSEN"
          }
        },
        "firstUserReference": {
          "referenceText": "test"
        },
        "secondUserReference": {
          "referenceText": "test2 example"
        },
        "thirdUserReference": {
          
        },
        "subscriptionType": {
          "type": "Bedrift Total",
          "accessType": "VOICE"
        },
        "suspended": false,
        "productOfferId": "15471",
        "account": {
          "firstAccountReference": {
            "referenceText": "FAST ANSATT 9M01"
          },
          "accountNumber": "9113713"
        },
        "dataSimMobileNumber": {
          "number": "580010087400",
          "secret": false
        },
        "status": "no",
        "barred": false,
        "ownerMasterId": "1680055"
      },
      {
        "id": "32228988",
        "mobileNumber": {
          "number": "48126167",
          "secret": false
        },
        "user": {
          "personUser": {
            "firstName": "KJELL MAGNE",
            "lastName": "ANDERSEN"
          }
        },
        "firstUserReference": {
          "referenceText": "test"
        },
        "secondUserReference": {
          "referenceText": "test2 example"
        },
        "thirdUserReference": {
          
        },
        "subscriptionType": {
          "type": "Bedrift Total",
          "accessType": "VOICE"
        },
        "suspended": false,
        "productOfferId": "15471",
        "account": {
          "firstAccountReference": {
            "referenceText": "TESTREF1"
          },
          "accountNumber": "9113712"
        },
        "status": "no",
        "barred": false,
        "ownerMasterId": "1680055"
      },
      {
        "id": "32230840",
        "mobileNumber": {
          "number": "90584170",
          "secret": false
        },
        "user": {
          "personUser": {
            "firstName": "KNUT INGE",
            "lastName": "ANDERSEN"
          }
        },
        "firstUserReference": {
          "referenceText": "test"
        },
        "secondUserReference": {
          "referenceText": "test2 example"
        },
        "thirdUserReference": {
          
        },
        "subscriptionType": {
          "type": "Bedrift Total",
          "accessType": "VOICE"
        },
        "suspended": false,
        "productOfferId": "15471",
        "account": {
          "firstAccountReference": {
            "referenceText": "TESTREF1"
          },
          "accountNumber": "9113712"
        },
        "status": "no",
        "barred": false,
        "ownerMasterId": "1680055"
      },
      {
        "id": "32229309",
        "mobileNumber": {
          "number": "48233819",
          "secret": false
        },
        "user": {
          "personUser": {
            "firstName": "LARS ANDREAS",
            "lastName": "ANDERSEN"
          }
        },
        "firstUserReference": {
          "referenceText": "test"
        },
        "secondUserReference": {
          "referenceText": "test2 example"
        },
        "thirdUserReference": {
          
        },
        "subscriptionType": {
          "type": "Bedrift Total",
          "accessType": "VOICE"
        },
        "suspended": false,
        "productOfferId": "15471",
        "account": {
          "firstAccountReference": {
            "referenceText": "TESTREF1"
          },
          "accountNumber": "9113712"
        },
        "status": "no",
        "barred": false,
        "ownerMasterId": "1680055"
      },
      {
        "id": "43687833",
        "mobileNumber": {
          "number": "90884289",
          "secret": false
        },
        "user": {
          "personUser": {
            "firstName": "LASSE",
            "lastName": "ANDERSEN"
          }
        },
        "firstUserReference": {
          "referenceText": "test"
        },
        "secondUserReference": {
          "referenceText": "test2 example"
        },
        "thirdUserReference": {
          
        },
        "subscriptionType": {
          "type": "Bedrift Total",
          "accessType": "VOICE"
        },
        "suspended": false,
        "productOfferId": "15471",
        "account": {
          "firstAccountReference": {
            "referenceText": "WE"
          },
          "accountNumber": "9114963"
        },
        "status": "no",
        "barred": false,
        "ownerMasterId": "1680055"
      },
      {
        "id": "32229447",
        "mobileNumber": {
          "number": "48191486",
          "secret": false
        },
        "user": {
          "personUser": {
            "firstName": "LEIF ERIK",
            "lastName": "ANDERSEN"
          }
        },
        "firstUserReference": {
          "referenceText": "test"
        },
        "secondUserReference": {
          "referenceText": "test2 example"
        },
        "thirdUserReference": {
          
        },
        "subscriptionType": {
          "type": "Bedrift Total",
          "accessType": "VOICE"
        },
        "suspended": false,
        "productOfferId": "15471",
        "account": {
          "firstAccountReference": {
            "referenceText": "TESTREF1"
          },
          "accountNumber": "9113712"
        },
        "status": "no",
        "barred": false,
        "ownerMasterId": "1680055"
      },
      {
        "id": "32222672",
        "mobileNumber": {
          "number": "41338901",
          "secret": false
        },
        "user": {
          "personUser": {
            "firstName": "LINDA THERESE ANGEN",
            "lastName": "ANDERSEN"
          }
        },
        "firstUserReference": {
          "referenceText": "test"
        },
        "secondUserReference": {
          "referenceText": "test2 example"
        },
        "thirdUserReference": {
          
        },
        "subscriptionType": {
          "type": "Bedrift Total",
          "accessType": "VOICE"
        },
        "suspended": false,
        "productOfferId": "15471",
        "account": {
          "firstAccountReference": {
            "referenceText": "TESTREF1"
          },
          "accountNumber": "9113712"
        },
        "twinSimMobileNumber": {
          "number": "46864903",
          "secret": false
        },
        "dataSimMobileNumber": {
          "number": "580000151904",
          "secret": false
        },
        "status": "no",
        "barred": false,
        "ownerMasterId": "1680055"
      },
      {
        "id": "32229487",
        "mobileNumber": {
          "number": "48195962",
          "secret": false
        },
        "user": {
          "personUser": {
            "firstName": "LISBETH",
            "lastName": "ANDERSEN"
          }
        },
        "firstUserReference": {
          "referenceText": "test"
        },
        "secondUserReference": {
          "referenceText": "test2 example"
        },
        "thirdUserReference": {
          
        },
        "subscriptionType": {
          "type": "Bedrift Total",
          "accessType": "VOICE"
        },
        "suspended": false,
        "productOfferId": "15471",
        "account": {
          "firstAccountReference": {
            "referenceText": "TESTREF1"
          },
          "accountNumber": "9113712"
        },
        "status": "no",
        "barred": false,
        "ownerMasterId": "1680055"
      },
      {
        "id": "32237402",
        "mobileNumber": {
          "number": "92280919",
          "secret": false
        },
        "user": {
          "personUser": {
            "firstName": "MATS GRØNNING",
            "lastName": "ANDERSEN"
          }
        },
        "firstUserReference": {
          "referenceText": "test"
        },
        "secondUserReference": {
          "referenceText": "test2 example"
        },
        "thirdUserReference": {
          
        },
        "subscriptionType": {
          "type": "Bedrift Total",
          "accessType": "VOICE"
        },
        "suspended": false,
        "productOfferId": "15471",
        "account": {
          "firstAccountReference": {
            "referenceText": "FAST ANSATT 9M01"
          },
          "accountNumber": "9113713"
        },
        "twinSimMobileNumber": {
          "number": "46957289",
          "secret": false
        },
        "dataSimMobileNumber": {
          "number": "46894167",
          "secret": false
        },
        "status": "no",
        "barred": false,
        "ownerMasterId": "1680055"
      },
      {
        "id": "32222493",
        "mobileNumber": {
          "number": "40479363",
          "secret": false
        },
        "user": {
          "personUser": {
            "firstName": "MERETE",
            "lastName": "ANDERSEN"
          }
        },
        "firstUserReference": {
          "referenceText": "test"
        },
        "secondUserReference": {
          "referenceText": "test2 example"
        },
        "thirdUserReference": {
          
        },
        "subscriptionType": {
          "type": "Bedrift Total",
          "accessType": "VOICE"
        },
        "suspended": false,
        "productOfferId": "15471",
        "account": {
          "firstAccountReference": {
            "referenceText": "TESTREF1"
          },
          "accountNumber": "9113712"
        },
        "twinSimMobileNumber": {
          "number": "99299746",
          "secret": false
        },
        "status": "no",
        "barred": false,
        "ownerMasterId": "1680055"
      },
      {
        "id": "32230034",
        "mobileNumber": {
          "number": "90277834",
          "secret": false
        },
        "user": {
          "personUser": {
            "firstName": "MONA",
            "lastName": "ANDERSEN"
          }
        },
        "firstUserReference": {
          "referenceText": "test"
        },
        "secondUserReference": {
          "referenceText": "test2 example"
        },
        "thirdUserReference": {
          
        },
        "subscriptionType": {
          "type": "Bedrift Total",
          "accessType": "VOICE"
        },
        "suspended": false,
        "productOfferId": "15471",
        "account": {
          "firstAccountReference": {
            "referenceText": "TESTREF1"
          },
          "accountNumber": "9113712"
        },
        "dataSimMobileNumber": {
          "number": "90244864",
          "secret": false
        },
        "status": "no",
        "barred": false,
        "ownerMasterId": "1680055"
      },
      {
        "id": "32237358",
        "mobileNumber": {
          "number": "92018099",
          "secret": false
        },
        "user": {
          "personUser": {
            "firstName": "NILS HOFSETH",
            "lastName": "ANDERSEN"
          }
        },
        "firstUserReference": {
          "referenceText": "test"
        },
        "secondUserReference": {
          "referenceText": "test2 example"
        },
        "thirdUserReference": {
          
        },
        "subscriptionType": {
          "type": "Bedrift Total",
          "accessType": "VOICE"
        },
        "suspended": false,
        "productOfferId": "15471",
        "account": {
          "firstAccountReference": {
            "referenceText": "FAST ANSATT 9M01"
          },
          "accountNumber": "9113713"
        },
        "status": "no",
        "barred": false,
        "ownerMasterId": "1680055"
      },
      {
        "id": "32246560",
        "mobileNumber": {
          "number": "48287696",
          "secret": false
        },
        "user": {
          "personUser": {
            "firstName": "NINA HAVSTAD",
            "lastName": "ANDERSEN"
          }
        },
        "firstUserReference": {
          "referenceText": "test"
        },
        "secondUserReference": {
          "referenceText": "test2 example"
        },
        "thirdUserReference": {
          
        },
        "subscriptionType": {
          "type": "Bedrift Total",
          "accessType": "VOICE"
        },
        "suspended": false,
        "productOfferId": "15471",
        "account": {
          "firstAccountReference": {
            "referenceText": "TESTREF1"
          },
          "accountNumber": "9113712"
        },
        "dataSimMobileNumber": {
          "number": "90367153",
          "secret": false
        },
        "status": "no",
        "barred": false,
        "ownerMasterId": "1680055"
      },
      {
        "id": "32237200",
        "mobileNumber": {
          "number": "93007913",
          "secret": false
        },
        "user": {
          "personUser": {
            "firstName": "ODD KARSTEN",
            "lastName": "ANDERSEN"
          }
        },
        "firstUserReference": {
          "referenceText": "test"
        },
        "secondUserReference": {
          "referenceText": "test2 example"
        },
        "thirdUserReference": {
          
        },
        "subscriptionType": {
          "type": "Bedrift Total",
          "accessType": "VOICE"
        },
        "suspended": false,
        "productOfferId": "15471",
        "account": {
          "firstAccountReference": {
            "referenceText": "FAST ANSATT 9M01"
          },
          "accountNumber": "9113713"
        },
        "status": "no",
        "barred": false,
        "ownerMasterId": "1680055"
      },
      {
        "id": "32241754",
        "mobileNumber": {
          "number": "95808421",
          "secret": false
        },
        "user": {
          "personUser": {
            "firstName": "OLE-STEINAR",
            "lastName": "ANDERSEN"
          }
        },
        "firstUserReference": {
          "referenceText": "test"
        },
        "secondUserReference": {
          "referenceText": "test2 example"
        },
        "thirdUserReference": {
          
        },
        "subscriptionType": {
          "type": "Bedrift Total",
          "accessType": "VOICE"
        },
        "suspended": false,
        "productOfferId": "15471",
        "account": {
          "firstAccountReference": {
            "referenceText": "WE"
          },
          "accountNumber": "9114963"
        },
        "twinSimMobileNumber": {
          "number": "46801990",
          "secret": false
        },
        "status": "no",
        "barred": false,
        "ownerMasterId": "1680055"
      },
      {
        "id": "32238497",
        "mobileNumber": {
          "number": "94815755",
          "secret": false
        },
        "user": {
          "personUser": {
            "firstName": "RUNE EDVARD",
            "lastName": "ANDERSEN"
          }
        },
        "firstUserReference": {
          "referenceText": "test"
        },
        "secondUserReference": {
          "referenceText": "test2 example"
        },
        "thirdUserReference": {
          
        },
        "subscriptionType": {
          "type": "Bedrift Total",
          "accessType": "VOICE"
        },
        "suspended": false,
        "productOfferId": "15471",
        "account": {
          "firstAccountReference": {
            "referenceText": "FAST ANSATT 9M01"
          },
          "accountNumber": "9113713"
        },
        "twinSimMobileNumber": {
          "number": "94144781",
          "secret": false
        },
        "dataSimMobileNumber": {
          "number": "97528665",
          "secret": false
        },
        "status": "no",
        "barred": false,
        "ownerMasterId": "1680055"
      },
      {
        "id": "32439090",
        "mobileNumber": {
          "number": "99241396",
          "secret": false
        },
        "user": {
          "personUser": {
            "firstName": "SANNA",
            "lastName": "ANDERSEN"
          }
        },
        "firstUserReference": {
          "referenceText": "test"
        },
        "secondUserReference": {
          "referenceText": "4503483202 test2"
        },
        "thirdUserReference": {
          
        },
        "subscriptionType": {
          "type": "Bedrift Total",
          "accessType": "VOICE"
        },
        "suspended": false,
        "productOfferId": "15471",
        "account": {
          "firstAccountReference": {
            "referenceText": "WE"
          },
          "accountNumber": "9114963"
        },
        "status": "no",
        "barred": false,
        "ownerMasterId": "1680055"
      },
      {
        "id": "40694702",
        "mobileNumber": {
          "number": "92633846",
          "secret": false
        },
        "user": {
          "personUser": {
            "firstName": "SONDRE MATRE",
            "lastName": "ANDERSEN"
          }
        },
        "firstUserReference": {
          "referenceText": "test"
        },
        "secondUserReference": {
          "referenceText": "test2 example"
        },
        "thirdUserReference": {
          
        },
        "subscriptionType": {
          "type": "Bedrift Total",
          "accessType": "VOICE"
        },
        "suspended": false,
        "productOfferId": "15471",
        "account": {
          "firstAccountReference": {
            "referenceText": "WE"
          },
          "accountNumber": "9114963"
        },
        "status": "no",
        "barred": false,
        "ownerMasterId": "1680055"
      },
      {
        "id": "32241369",
        "mobileNumber": {
          "number": "95723479",
          "secret": false
        },
        "user": {
          "personUser": {
            "firstName": "STEIN KRISTIAN",
            "lastName": "ANDERSEN"
          }
        },
        "firstUserReference": {
          "referenceText": "test"
        },
        "secondUserReference": {
          "referenceText": "test2 example"
        },
        "thirdUserReference": {
          
        },
        "subscriptionType": {
          "type": "Bedrift Total",
          "accessType": "VOICE"
        },
        "suspended": false,
        "productOfferId": "15471",
        "account": {
          "firstAccountReference": {
            "referenceText": "WE"
          },
          "accountNumber": "9114963"
        },
        "dataSimMobileNumber": {
          "number": "580000270877",
          "secret": false
        },
        "data2SimMobileNumber": {
          "number": "580000010659",
          "secret": false
        },
        "status": "no",
        "barred": false,
        "ownerMasterId": "1680055"
      },
      {
        "id": "44496201",
        "mobileNumber": {
          "number": "90933811",
          "secret": false
        },
        "user": {
          "personUser": {
            "firstName": "STEIN-IVAR FUGLAAS",
            "lastName": "ANDERSEN"
          }
        },
        "firstUserReference": {
          "referenceText": "test"
        },
        "secondUserReference": {
          "referenceText": "test2 example"
        },
        "thirdUserReference": {
          
        },
        "subscriptionType": {
          "type": "Bedrift Total",
          "accessType": "VOICE"
        },
        "suspended": false,
        "productOfferId": "15471",
        "account": {
          "firstAccountReference": {
            "referenceText": "WE"
          },
          "accountNumber": "9114963"
        },
        "status": "no",
        "barred": false,
        "ownerMasterId": "1680055"
      }
]