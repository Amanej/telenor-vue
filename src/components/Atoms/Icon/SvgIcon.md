Icon component example:

```js
    <Icon
        path="M30 6H15l-3-3H2C.9 3 0 3.9 0 5v21c0 1.1.9 2 2 2h28c1.1 0 2-.9 2-2V8c0-1.1-.9-2-2-2zM2 5h9.1l2.5 2.4.6.6H30v2H2V5zm0 21V12h28v14H2z" 
        size="30" 
        color="black" 
        viewBox="0 0 32 32" 
        style="margin: auto 8px;" 
        v-on:click="alert('Ikon clicked');"
    />
```