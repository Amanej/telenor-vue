# Configurable list

This is a configurable list, that uses a set of filters, keys and items to create a filterable list.

## Expected data structure

### Keys

The keys are necessary to create the headers for the list item.

They are a simple array, but they need to correspond to the keys in the items that will be displayed.

### Items

These are the items, they are simply any object but where the keys of the object where the values will be display must correspond to the keys provided. The also need a selected field, set to values, if they can be selected.

```
[
    {
        "Mobil":"Asus",
        "Farge":"Rød",
        "Pris":299,
        "Abonnement":"Bedrift",
        "selected":false
    },
    {
        "Mobil":"Blackberry",
        "Farge":"Rød",
        "Pris":299,
        "Abonnement":"Bedrift",
        "selected":false
    },
    {
        "Mobil":"iPhone",
        "Farge":"Rød",
        "Pris":299,
        "Abonnement":"Bedrift",
        "selected":false
    },
    {
        "Mobil":"Pixel",
        "Farge":"Rød",
        "Pris":299,
        "Abonnement":"Bedrift",
        "selected":false
    },
    {
        "Mobil":"Huawei",
        "Farge":"Rød",
        "Pris":299,
        "Abonnement":"Bedrift",
        "selected":false
    },
    {
        "Mobil":"Samsung",
        "Farge":"Rød",
        "Pris":299,
        "Abonnement":"Bedrift",
        "selected":false
    },
    {
        "Mobil":"Asus",
        "Farge":"Rød",
        "Pris":299,
        "Abonnement":"Bedrift",
        "selected":false
    },
    {
        "Mobil":"Blackberry",
        "Farge":"Rød",
        "Pris":299,
        "Abonnement":"Bedrift",
        "selected":false
    }
]
```


### Filters - optional

```
[
    {
      key: 'Mobil',
      values:[
        {
          value: 'iPhone',selected: false
        },
        {
          value: 'Samsung',selected: false
        },
        {
          value: 'Huawei',selected: false
        },
        {
          value: 'Blackberry',selected: false
        },
        {
          value: 'Asus',selected: false
        },
        {
          value: 'Pixel',selected: false
        }
      ]
    }
]
```

## TODO

1. Handle nested data from external sources
    * Use slots 
2. 