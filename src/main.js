import Vue from 'vue'
import VueRouter from 'vue-router'
import VeeValidate, { Validator } from 'vee-validate'
import no from 'vee-validate/dist/locale/nb_NO';
import store from './store'

import App from './App.vue'
import Home from './Home.vue'
import ListsPage from './Pages/Lists/index.vue'
import PaginatedListsPage from './Pages/Lists/Paginated.vue'
import PaginatedVuex from './Pages/Lists/PaginatedVuex.vue'
import PaginatedConfigured from './Pages/Lists/PaginatedConfigured.vue'
import UserInfo from './Pages/User/Info.vue'
import FromTo from './Pages/App/Abonnement/Endre'


// App pages example
import Abonnementer from './Pages/App/Abonnementer.vue'
import NyttAbonnement from './Pages/App/Abonnement/Nytt.vue'
import Simkort from './Pages/App/Simkort.vue'
import Kontoer from './Pages/App/Kontoer.vue'
import NewAccount from './Pages/App/Konto/New.vue'
import Lokalasjoner from './Pages/App/Lokalasjoner.vue'
import Administratorer from './Pages/App/Administratorer.vue'
import NewAdministrator from './Pages/App/Administratorer/New.vue'
import Selskaper from './Pages/App/Selskaper.vue'



// Docs
import Documentation from './Pages/Documentation/Main.vue'
import DocumentationSelect from './Pages/Documentation/DocumentationSelect.vue'

import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.config.productionTip = false

console.log('back ',process.env,process.env.API_URL,process.env.LOGIN_URL);

Vue.use(VueRouter);
Vue.use(VueAxios, axios);
Vue.use(VeeValidate);

Validator.localize('no',no);

const router = new VueRouter({
  mode: 'history',
  routes: [
    { path: '/', component: App },
    { path: '/list', component: ListsPage },
    { path: '/list/paginated', component: PaginatedListsPage },
    { path: '/list/paginated-vuex', component: PaginatedVuex },
    { path: '/list/paginated-config', component: PaginatedConfigured },
    { path: '/elements/select', component: DocumentationSelect },
    { path: '/bruker/:id', component: UserInfo },
    { path: '/abonnementer', component: Abonnementer },
    { path: '/opprett-abonnement', component: NyttAbonnement },
    { path: '/fra-til-abonnement', component: FromTo },
    //{ path: 'eksporter-abonnement', component: Eksporter},
    { path: '/simkort', component: Simkort },
    { path: '/kontoer', component: Kontoer },
    { path: '/kontoer/ny', component: NewAccount },
    { path: '/lokalasjoner', component: Lokalasjoner },
    { path: '/administratorer', component: Administratorer },
    { path: '/administratorer/ny', component: NewAdministrator },
    { path: '/selskaper', component: Selskaper },
    { path: '/documentation', component: Documentation}
  ]
})

new Vue({
  router,
  store,
  render: h => h(Home),
}).$mount('#app')
