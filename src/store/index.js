import Vue from 'vue'
import Vuex from 'vuex'

import subscriptions from './modules/subscriptions'
import simcards from './modules/simcards'
import accounts from './modules/accounts'
import locations from './modules/locations'
import admins from './modules/admins'
import companies from './modules/companies'
import auth from './modules/auth'

Vue.use(Vuex);

const store = new Vuex.Store({
    modules: {
        subscriptions,
        simcards,
        accounts,
        locations,
        admins,
        companies,
        auth
    },
})

export default store