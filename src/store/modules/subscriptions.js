import api from '../../api/index.js'

const state = {
    all: [],
    selectedSubscriptions: [],
    page: 0
};


const getters = {
    getSelectedSubscriptions() {
        return []
    }
}

// actions
const actions = {
    getAll({state, commit }) {
      return api.subscriptions.get(state.page)
      .then(subs => {
        if(!state.page) {
          commit('setSubscriptions', subs);
          return subs
        }
        commit('appendSubscriptions', subs);
        return subs
      })
      .catch(e => {
          // Error message
          console.error(e);
      })
    },
    searchAll({commit},query) {
      return api.subscriptions.search(query)
      .then(subs => {
        commit('setSubscriptions', subs);
        return subs
      })
      .catch(e => {
          // Error message
          console.error(e);
      })
    }
}

// mutations
const mutations = {
    setSubscriptions (state, subscriptions) {
      state.all = subscriptions
    },
    appendSubscriptions(state,subscriptions) {
      state.all = state.all.concat(subscriptions);
    },
    select(state,subscription) {
      console.log('selectSubscription called');
      state.selectedSubscriptions.push(subscription);
    },
    updatePage(state) {
        state.page += 1
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}