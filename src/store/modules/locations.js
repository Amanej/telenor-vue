import api from '../../api/index.js'

const state = {
    all: [],
    selectedLocations: [],
    page: 0
};


const getters = {
}

// actions
const actions = {
    getAll({state, commit }) {
      api.locations.get(state.page)
      .then(locations => {
        let formattedLocations = locations.map(account => {
          account.id = account.farId;
          return account
        });
        console.log(' formattedLocations ',formattedLocations);
        commit('appendLocations', formattedLocations);
      })
      .catch(e => {
          // Error message
          console.error(e);
      })
    },
    searchAll({commit},query) {
      api.locations.search(query)
      .then(locations => {
        let formattedLocations = locations.map(account => {
          account.id = account.farId;
          return account
        });
        commit('setLocations', formattedLocations);
      })
      .catch(e => {
          // Error message
          console.error(e);
      })
    }
}

// mutations
const mutations = {
    setLocations (state, locations) {
      state.all = locations
    },
    appendLocations(state,locations) {
      state.all = state.all.concat(locations);
    },
    select(state,account) {
      console.log('selectAccount called');
      state.selectedLocations.push(account);
    },
    updatePage(state) {
        state.page += 1
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}