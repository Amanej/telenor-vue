import api from '../../api/index.js'

const state = {
    all: [],
    selectedSimCards: [],
    page: 0
};


const getters = {
    getSelectedSimcards() {
        return []
    }
}

// actions
const actions = {
    getAll({state, commit }) {
      api.simcards.get(state.page)
      .then(simcards => {
        let formattedSims = simcards.map(simcard => {
          simcard.id = simcard.imsi;
          return simcard
        });
        console.log(' formattedSims ',formattedSims);
        commit('appendSimcards', formattedSims);
      })
      .catch(e => {
          // Error message
          console.error(e);
      })
    },
    searchAll({commit},query) {
      api.simcards.search(query)
      .then(simcards => {
        let formattedSims = simcards.map(simcard => {
          simcard.id = simcard.imsi;
          return simcard
        });
        commit('setSimcards', formattedSims);
      })
      .catch(e => {
          // Error message
          console.error(e);
      })
    }
}

// mutations
const mutations = {
    setSimcards (state, simcards) {
      state.all = simcards
    },
    appendSimcards(state,simcards) {
      state.all = state.all.concat(simcards);
    },
    select(state,simcard) {
      console.log('selectSubscription called');
      state.selectedSimCards.push(simcard);
    },
    updatePage(state) {
        state.page += 1
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}