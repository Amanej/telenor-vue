import api from '../../api/index.js'

const state = {
    all: [],
    selectedCompanies: [],
    page: 0
};


const getters = {
}

// actions
const actions = {
    getAll({state, commit }) {
      return api.companies.get(state.page)
      .then(companies => {
        if(!state.page) {
          commit('setCompanies', companies);
          return companies
        } else {
          commit('appendCompanies', companies);
        }
      })
      .catch(e => {
          // Error message
          console.error(e);
      })
    },
    searchAll({commit},query) {
      return api.companies.search(query)
      .then(subs => {
        commit('setCompanies', subs);
      })
      .catch(e => {
          // Error message
          console.error(e);
      })
    }
}

// mutations
const mutations = {
    setCompanies (state, companies) {
      state.all = companies
    },
    appendCompanies(state,companies) {
      state.all = state.all.concat(companies);
    },
    select(state,company) {
      console.log('selectAccount called');
      state.selectedAccounts.push(company);
    },
    updatePage(state) {
        state.page += 1
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}