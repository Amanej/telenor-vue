import api from '../../api/index.js'

const state = {
    tokens: {
        csfrToken: '',
        ssoSessionId: ''
    },
    user: {}
};

/*
    csrfToken: "7e698053-1e4e-4def-aeb3-1566d81d0d8a"
    ssoSessionId: "SMS_ast-sgt-ga01_ast-sgt-ga01_a6c781::1f61bbfca2197c3f6064e2306ac09576"
    userProfile:
        accessToFileSharing: true
        accessToFixed: true
        accessToGlobalMobile: true
        accessToReports: true
        accessToTicketing: true
        agreementAccepted: true
        apiAdminAccess: true
        companyKurtId: "1082916"
        companyName: "EQUINOR ASA"
        dateOfBirth: "1988-10-08"
        email: "kulbhaskar.pandey@accenture.com"
        firstName: "DANIEL"
        hasAgreementUpdated: false
        id: "braathen"
        industryType: "Shipping/Olje/Offshore"
        lang: "en_US"
        lastLoginDate: "2019-04-12"
        lastName: "BRÅTHEN"
        masterId: "1680055"
        mobileFunctionalityAccess: true
        mobileNumber: "99999992"
        orderAdminAccess: true
        organizationNumber: "923609016"
        pilotAccess: false
        reportTutorialShown: true
        segmentId: "Store Bedrifter"
        showGdprNotification: false
        superUser: true
        tutorialShown: true
        userKurtId: "6178002"
        viewInvoiceAccess: true
        viewSecretNumberAccess: true
        writeAccessFixed: true
        writeAccessMobile: true
*/

const getters = {
    userName(state) {
        if(!state.user.firstName && !state.user.lastName) {
            return
        }
        const firstName = state.user.firstName;
        const lastName = state.user.lastName;

        const formattedFirstName = firstName.charAt(0)+firstName.substring(1,firstName.length).toLowerCase();
        const formattedLastName = lastName.charAt(0)+lastName.substring(1,lastName.length).toLowerCase(); //lastName.toLowerCase().charAt(0).toUpperCase();
        //const name = state.user.firstName.toLowerCase().charAt(0).toUpperCase() + state.user.lastName;
        return `${formattedFirstName} ${formattedLastName}`;
    }
}

// actions
const actions = {
    isUserLoggedIn({commit}) {
        // Query
        api.auth.get()
        .then(({data}) => {
            // Set details
            console.log(' isUserLoggedIn ',data);
            commit('setUser', data.userProfile);
            commit('setTokens', 
                {
                    csfrToken: data.csfrToken,
                    ssoSessionId: data.ssoSessionId
                }
            );
        })
        .catch(e => {
            console.error(' isUserLoggedIn failed ',e);
        });
    }
}

// mutations
const mutations = {
    setUser(state, user) {
      state.user = user
    },
    setTokens(state,tokens) {
      state.tokens.csfrToken = tokens.csfrToken;
      state.tokens.ssoSessionId = tokens.ssoSessionId;
    },
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}