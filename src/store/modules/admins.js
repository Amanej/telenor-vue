import api from '../../api/index.js'

const state = {
    all: [],
    selectedAdmins: [],
    page: 0
};


const getters = {
}

// actions
const actions = {
    getAll({state, commit }) {
      api.admins.get(state.page)
      .then(admins => {
        let formattedAdmins = admins.map(admin => {
            admin.id = admin.tnuId;
          return admin
        });
        console.log(' formattedAdmins ',formattedAdmins);
        commit('appendAdmins', formattedAdmins);
      })
      .catch(e => {
          // Error message
          console.error(e);
      })
    },
    searchAll({commit},query) {
      api.admins.search(query)
      .then(admins => {
        let formattedAdmins = admins.map(admin => {
          admin.id = admin.tnuId;
          return admin
        });
        commit('setAdmins', formattedAdmins);
      })
      .catch(e => {
          // Error message
          console.error(e);
      })
    }
}

// mutations
const mutations = {
    setAdmins (state, admins) {
      state.all = admins
    },
    appendAdmins(state,admins) {
      state.all = state.all.concat(admins);
    },
    select(state,admin) {
      console.log('selectAccount called');
      state.selectedAdmins.push(admin);
    },
    updatePage(state) {
        state.page += 1
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}