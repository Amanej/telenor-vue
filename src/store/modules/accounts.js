import api from '../../api/index.js'

const state = {
    all: [],
    selectedAccounts: [],
    page: 0
};


const getters = {
}

// actions
const actions = {
    getAll({state, commit }) {
      api.accounts.get(state.page)
      .then(accounts => {
        let formattedAccounts = accounts.map(account => {
          account.id = account.accountNumber;
          return account
        });
        console.log(' formattedSims ',formattedAccounts);
        commit('appendAccounts', formattedAccounts);
      })
      .catch(e => {
          // Error message
          console.error(e);
      })
    },
    searchAll({commit},query) {
      api.accounts.search(query)
      .then(subs => {
        commit('setAccounts', subs);
      })
      .catch(e => {
          // Error message
          console.error(e);
      })
    }
}

// mutations
const mutations = {
    setAccounts (state, accounts) {
      state.all = accounts
    },
    appendAccounts(state,accounts) {
      state.all = state.all.concat(accounts);
    },
    select(state,account) {
      console.log('selectAccount called');
      state.selectedAccounts.push(account);
    },
    updatePage(state) {
        state.page += 1
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}