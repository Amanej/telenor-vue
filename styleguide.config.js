module.exports = {
	// set your styleguidist configuration here
	title: 'Default Style Guide',
	//components: 'src/components/**/[A-Z]*.vue',
	defaultExample: true,
	sections: [
		{
			name: 'Atoms',
			components: 'src/components/Atoms/**/[A-Z]*.vue'
		},
		{
			name: 'Molecules',
			components: 'src/components/Molecules/**/[A-Z]*.vue'
		},
		{
			name: 'Organisms',
			components: 'src/components/Organisms/**/[A-Z]*.vue'
		}
	]
	// webpackConfig: {
	//   // custom config goes here
	// }
}
