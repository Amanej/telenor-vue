module.exports = {
    devServer: {
      port: 3000,
      proxy: {
        '^/bedrift/minbedrift/api': {
          target: 'http://mb2-backend-local.test-dc.corp.telenor.no', // 'http://mb2-backend-sit.test-dc.corp.telenor.no/', //'http://mb2-local-backend.test-dc.corp.telenor.no/',
          ws: true,
          changeOrigin: true
        }
      }
    }
  }